<?php

return array(
    'router' => array(
        'routes' => array(
            'sanyaform' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/sanyaform[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'SanyaForm\Controller\FormSubmit',
                        'action' => 'post',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'SanyaForm\Controller\FormSubmit' => 'SanyaForm\Controller\FormSubmitController',
        ),
    ),
);
