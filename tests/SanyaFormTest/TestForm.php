<?php

namespace SanyaFormTest;

use SanyaForm\Form;

/**
 * Description of TestForm
 *
 * @author sanyatuning
 */
class TestForm extends Form {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->add(array(
            'name' => 'text',
            'type' => 'text',
        ));
    }

}
