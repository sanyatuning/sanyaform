<?php

namespace SanyaFormTest;

use SanyaFormTest\TestForm as Form;

/**
 * Description of ListPluralizerTest
 *
 * @author sanyatuning
 */
class FormTest extends \PHPUnit_Framework_TestCase {

    protected static $formid;

    public function testFormSave() {
        $test = 'Hello World!';
        $form = new Form();
        $form->setData(array(
            'text' => $test,
        ));
        $this->assertTrue($form->isValid());
        $form->save();

        $form2 = Form::getForm($form->getFormId());
        $this->assertTrue($form2 instanceof Form);
        $this->assertSame($test, $form2->get('text')->getValue());
    }

    public function testFormClear() {
        $form1 = new Form();
        $form1->save();

        $formid = $form1->getFormId();

        $this->assertTrue(Form::getForm($formid) instanceof Form);

        $form1->clear();

        $this->assertTrue(Form::getForm($formid) === NULL);
    }

    //public function 
}
