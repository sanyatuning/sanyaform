<?php

namespace SanyaForm;

use Zend\Form\Form as ZendForm;
use Zend\Session\Container;
use Zend\View\Renderer\RendererInterface;
use Zend\Log\LoggerAwareInterface;
use Zend\Log\LoggerInterface;

/**
 * Description of Form
 *
 * @author sanya
 */
class Form extends ZendForm implements LoggerAwareInterface {

    const MSG_SUCCESS = 0;
    const MSG_INVALID = 1;
    const MSG_ERROR = 2;
    const FORM_NOT_FOUND = 'Form not found in session.';

    /**
     * @var \Zend\Session\Container
     */
    protected static $session;
    protected $_options;
    protected $_formid;
    protected $messages = array();
    /**
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * 
     * @return Container
     */
    protected static function getSession() {
        if (is_null(self::$session)) {
            self::$session = new Container(__NAMESPACE__);
        }
        return self::$session;
    }

    public static function getForm($formid) {
        $session = self::getSession();
        if (isset($session[$formid])) {
            $formArr = $session[$formid];
            $class = $formArr['class'];
            $name = $formArr['name'];
            $options = $formArr['options'];
            $form = new $class($name, $options);
            $form->setMessages($formArr['messages']);
            if (is_object($formArr['data'])) {
                $form->bind($formArr['data']);
            } else {
                if (isset($formArr['data'])) {
                    $form->setData($formArr['data']);
                }
            }
            return $form;
        }
        return NULL;
    }

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/sanyaform');
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'formid',
        ));
    }

    /**
     * 
     * @param string|int|Object $message
     * @return \SanyaForm\Form
     */
    public function addMessage($message) {
        if (is_numeric($message)) {
            $this->messages[] = $this->getMessage($message);
        } else {
            $this->messages[] = $message;
        }
        return $this;
    }

    public function getMessages($elementName = null) {
        if ($elementName == 'Form') {
            return $this->messages;
        }
        $messages = parent::getMessages($elementName);
        $messages['Form'] = $this->messages;
        return $messages;
    }

    public function setMessages($messages) {
        if (isset($messages['Form'])) {
            $this->messages = $messages['Form'];
            unset($messages['Form']);
        }
        return parent::setMessages($messages);
    }

    public function getFormId() {
        if (!isset($this->_formid)) {
            $id = $this->get('Id');
            $this->_formid = md5(get_class($this) . ($id ? $id->getValue() : ''));
        }
        return $this->_formid;
    }

    public function save($clear = false) {
        $formid = $this->getFormId();
        $s = array(
            'class' => get_class($this),
            'name' => $this->getName(),
            'options' => $this->getOptions(),
            'messages' => $clear ? array('Form' => $this->getMessages('Form')) : $this->getMessages(),
            'data' => null,
        );
        if (!$clear && $this->hasValidated()) {
            $data = $this->getData();
            $s['data'] = is_array($data) ? $data : $data->toArray();
        }
        $session = self::getSession();
        $session[$formid] = $s;
        return $this;
    }

    public function clear() {
        $formid = $this->getFormId();
        $session = self::getSession();
        unset($session[$formid]);
    }

    public function prepare() {
        $formid = $this->getFormId();
        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'security' . $formid,
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 3600,
                )
            )
        ));
        $session = self::getSession();
        if (isset($session[$formid])) {
            $s = $session[$formid];
            if (!empty($s['messages'])) {
                $this->setMessages($s['messages']);
                $s['messages'] = array();
            }
            if (!empty($s['data'])) {
                $this->setData($s['data']);
                $s['data'] = array();
            }
            $session[$formid] = $s;
        }

        parent::prepare();
        $this->get('formid')->setValue($formid);
        if (!isset($session[$formid])) {
            $this->save();
        }
        return $this;
    }

    public function submit(array $data) {
        $clear = false;
        try {
            $this->setData($data);
            if ($this->isValid()) {
                $success = $this->process();
                if ($success) {
                    $clear = true;
                }
                $msg_type = $success ? self::MSG_SUCCESS : self::MSG_ERROR;
            } else {
                $success = FALSE;
                $msg_type = self::MSG_INVALID;
            }
            $result = array(
                'result' => $success,
            );
        } catch (\Exception $e) {
            if ($this->logger) {
                $this->logger->err($e->getMessage(),$e);
            }
            $msg_type = self::MSG_ERROR;
            $result = array(
                'result' => FALSE,
                'exception' => array(
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'trace' => $e->getTrace(),
                ),
                'dataIn' => $data,
            );
        }
        $this->addMessage($msg_type);
        $this->save($clear);
        return $result;
    }

    protected function process() {
        throw new \Exception('Not implemented function: ' . get_class($this) . '->' . __FUNCTION__);
    }

    /**
     * @param int $msg_type
     * @return string
     */
    protected function getMessage($msg_type) {
        switch ($msg_type) {
            case self::MSG_SUCCESS:
                return 'Success';
            case self::MSG_INVALID:
                return 'Invalid form input';
            case self::MSG_ERROR:
                return 'Internal error';
        }
        return 'Unknow message';
    }

    /**
     * Implement this method to use Ajax form submit method
     * 
     * @param \Zend\View\Renderer\RendererInterface $renderer
     * @throws \Exception
     */
    public function render(RendererInterface $renderer) {
        throw new \Exception('Not implemented function: ' . get_class($this) . '->' . __FUNCTION__);
    }

    public function setLogger(\Zend\Log\LoggerInterface $logger) {
        $this->logger = $logger;
    }

}
