<?php

namespace SanyaForm\Form\Element;

/**
 * Description of Number
 *
 * @author sanyatuning
 */
class Number extends \Zend\Form\Element\Number {
    public function getInputSpecification() {
        $spec = parent::getInputSpecification();
        $spec['required'] = false;
        return $spec;
    }
}

