<?php

namespace SanyaForm\Controller;

use \Exception;
use SanyaForm\Form;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\PhpEnvironment\Request as HttpRequest;
use Zend\Http\PhpEnvironment\Response as HttpResponse;

/**
 * Description of FormSubmitController
 *
 * @author sanyatuning
 */
class FormSubmitController extends AbstractActionController {

    const REDIRECT_CODE = 303;

    protected function reloadPage(HttpRequest $request, HttpResponse $response) {
        $location = new \Zend\Http\Header\Location();
        $location->setUri($request->getServer('HTTP_REFERER'));
        $response->setStatusCode(self::REDIRECT_CODE);
        $response->getHeaders()->addHeader($location);
        $response->send();
        exit;
    }

    public function onModulesLoaded(ModuleEvent $event) {
        $sm = $event->getParam('ServiceManager');
        $request = $sm->get('Request');
        if ($request instanceof HttpRequest && $request->isPost()) {
            $post = $request->getPost()->toArray();
            if (isset($post['formid'])) {
                $form = Form::getForm($post['formid']);
                if ($form instanceof Form) {
                    $form->processHttp($post);
                    $this->reloadPage($request, $sm->get('Response'));
                    return;
                }
            }
        }
    }

    public function ajaxAction() {
        $request = $this->getRequest();
        $result = array();
        if ($request instanceof HttpRequest && $request->isPost()) {
            $post = $request->getPost()->toArray();
            if (isset($post['formid'])) {
                $form = Form::getForm($post['formid']);
                if ($form instanceof Form) {
                    $result = $form->submit($post);
                    try {
                        $renderer = $this->getServiceLocator()->get('viewRenderer');
                        $form->prepare();
                        $result['form'] = $form->render($renderer);
                    } catch (Exception $e) {
                        $form->addMessage($e->getMessage());
                    }
                    $result['errors'] = $form->getMessages();
                } else {
                    $result['errors'] = array('Form' => array('Form is not a valid SanyaForm'));
                }
            } else {
                $result['errors'] = array('Form' => array('Form not found'));
            }
        } else {
            $result['errors'] = array('Form' => array('Bad method. Use POST insted of'));
        }
        echo json_encode($result);
        exit;
    }

    public function postAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request instanceof HttpRequest && $request->isPost()) {
            $post = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            if (isset($post['formid'])) {
                $form = Form::getForm($post['formid']);
                if ($form instanceof Form) {
                    $form->submit($post);
                }
            }
        }
        $this->reloadPage($request, $response);
        exit;
    }

}

